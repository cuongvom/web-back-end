using Microsoft.EntityFrameworkCore;
namespace web.domain.Context
{
    public class FamilyDbContext : DbContext
    {
        public DbSet<Entity.AppRole> AppRoles { get; set; }
        public DbSet<Entity.AppUser> AppUsers { get; set; }
        public DbSet<Entity.Province> Provinces { get; set; }
        public DbSet<Entity.District> Districts { get; set; }
        public DbSet<Entity.Family> Families { get; set; }
        public DbSet<Entity.People> Peoples { get; set; }
        public DbSet<Entity.FamilyRelationship> FamilyRelationships { get; set; }
        public DbSet<Entity.InsertLogForPeople> InsertLogForPeoples { get; set; }
        
        public FamilyDbContext(DbContextOptions<FamilyDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(FamilyDbContext).Assembly);
        }
    }
}