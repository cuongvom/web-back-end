using AutoMapper;
using web.application.api.Model;
using web.domain.Entity;

namespace web.domain.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<People,PeopleDto>().ForMember(peopleDto=>peopleDto.Address,x=>
            x.MapFrom(f=>$"{f.District.Name} District,{f.District.Province.Name} Province"));
        }
    }
}