namespace web.domain.Entity
{
    public class District
    {
        public int Id { get; set; }
        public int ProvinceId { get; set; }
        public string Name { get; set; }

         public Province Province { get; set; }
    }
}