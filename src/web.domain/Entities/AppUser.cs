﻿namespace web.domain.Entity
{
    public class AppUser
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }

        public AppRole AppRole { get; set; }
    }
}
