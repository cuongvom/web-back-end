namespace web.domain.Entity
{
    public class FamilyRelationship
    {
        public int PeopleId { get; set; }
        public int PeopleIdReId { get; set; }
        public string RelationshipName { get; set; }
    }
}