namespace web.domain.Entity
{
    public class Province
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}