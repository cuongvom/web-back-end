namespace web.domain.Entity
{
    public class AppRole
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
    }
}