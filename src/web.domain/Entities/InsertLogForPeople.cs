using System;
namespace web.domain.Entity
{
    public class InsertLogForPeople
    {
        public int Id { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string TriggerAction { get; set; }
    }
}