namespace web.domain.Entity
{
    public class People
    {
        public int Id { get; set; }
        public int DistrictId { get; set; }
        public int FamilyId { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public bool IsMarried { get; set; }

        public District District { get; set; }
    }
}