namespace web.domain.Entity
{
    public class Family
    {
        public int Id { get; set; }
        public int HeadOfAHouseholdId { get; set; }
    }
}