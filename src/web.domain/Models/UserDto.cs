namespace web.domain.Model
{
    public class UserDto
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
    }
}