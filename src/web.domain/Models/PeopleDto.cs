namespace web.application.api.Model
{
    public class PeopleDto
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
    }
}