namespace web.domain.Models.RequestModel
{
    public class UserRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}