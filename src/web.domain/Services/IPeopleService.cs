using System.Threading.Tasks;
using System.Collections.Generic;
using web.application.api.Model;

namespace web.domain.Service
{
    public interface IPeopleService
    {
        IEnumerable<PeopleDto> GetAll();
    }
}