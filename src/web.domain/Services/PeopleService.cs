using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using web.application.api.Model;
using web.domain.Entity;
using web.domain.Repository;

namespace web.domain.Service
{
    public class PeopleService : IPeopleService
    {
        private readonly IPeopleRepositoryExtension _peopleRepository;
        private readonly IMapper _mapper;

        public PeopleService(IPeopleRepositoryExtension peopleRepository, IMapper mapper)
        {
            _peopleRepository = peopleRepository;
            _mapper = mapper;
        }

        public IEnumerable<PeopleDto> GetAll()
        {
            var listTemp = _peopleRepository.GetAllAsync().Result;
            if (listTemp == null)
                return null;
            var listPeople = _mapper.Map<IEnumerable<People>, IEnumerable<PeopleDto>>(listTemp);
            return listPeople;
        }
    }
}