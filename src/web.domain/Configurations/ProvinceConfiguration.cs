using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using web.domain.Entity;

namespace web.domain.Configurations
{
    public class ProvinceConfiguration : IEntityTypeConfiguration<web.domain.Entity.Province>
    {
        public void Configure(EntityTypeBuilder<Province> builder)
        {
            builder.ToTable("Province");
            builder.HasKey(x => x.Id);
        }
    }
}