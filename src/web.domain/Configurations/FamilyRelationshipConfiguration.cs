using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using web.domain.Entity;

namespace web.domain.Configurations
{
    public class FamilyRelationshipConfiguration : IEntityTypeConfiguration<web.domain.Entity.FamilyRelationship>
    {
        public void Configure(EntityTypeBuilder<FamilyRelationship> builder)
        {
            builder.ToTable("FamilyRelationship");
            builder.HasKey(x => x.PeopleId);
        }
    }
}