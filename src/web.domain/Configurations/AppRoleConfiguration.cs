using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace web.domain.Configurations
{
    public class AppRoleConfiguration : IEntityTypeConfiguration<web.domain.Entity.AppRole>
    {
        public void Configure(EntityTypeBuilder<Entity.AppRole> builder)
        {
            builder.ToTable("AppRole");
            builder.HasKey(x => x.Id);
        }
    }
}