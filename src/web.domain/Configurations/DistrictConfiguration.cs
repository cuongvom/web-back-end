using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using web.domain.Entity;

namespace web.domain.Configurations
{
    public class DistrictConfiguration : IEntityTypeConfiguration<web.domain.Entity.District>
    {
        public void Configure(EntityTypeBuilder<District> builder)
        {
            builder.ToTable("District");
            builder.HasKey(x => x.Id);
        }
    }
}