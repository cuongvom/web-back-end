using Microsoft.EntityFrameworkCore;

namespace web.domain.Configurations
{
    public class AppUserConfiguration : IEntityTypeConfiguration<web.domain.Entity.AppUser>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Entity.AppUser> builder)
        {
            builder.ToTable("AppUser");
            builder.HasKey(x=>x.Id);
        }
    }
}