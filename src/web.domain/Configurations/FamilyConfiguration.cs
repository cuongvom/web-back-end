using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using web.domain.Entity;

namespace web.domain.Configurations
{
    public class FamilyConfiguration : IEntityTypeConfiguration<web.domain.Entity.Family>
    {
        public void Configure(EntityTypeBuilder<Family> builder)
        {
            builder.ToTable("Family");
            builder.HasKey(x => x.Id);
        }
    }
}