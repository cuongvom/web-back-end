using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using web.domain.Entity;

namespace web.domain.Configurations
{
    public class PeopleConfiguration : IEntityTypeConfiguration<web.domain.Entity.People>
    {
        public void Configure(EntityTypeBuilder<People> builder)
        {
            builder.ToTable("People");
            builder.HasKey(x=>x.Id);
        }
    }
}