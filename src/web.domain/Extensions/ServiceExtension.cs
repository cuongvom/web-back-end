using Microsoft.Extensions.DependencyInjection;
using web.domain.Service;

namespace web.domain.Extension
{
    public static class ServiceExtension
    {
        public static void AddServiceExtension(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IPeopleService, PeopleService>();
        }
    }
}