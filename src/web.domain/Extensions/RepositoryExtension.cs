using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using web.domain.Context;
using web.domain.Repository;

namespace web.domain.Extension
{
    public static class RepositoryExtension
    {
        public static void AddRepositoryService(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddDbContext<FamilyDbContext>((service, options) =>
            {
                var configuration = service.GetService<IConfiguration>();
                var familyConnectionString = configuration.GetConnectionString("Family");
                options.UseSqlServer(familyConnectionString);
            });
            serviceCollection.AddScoped<IPeopleRepositoryExtension, PeopleRepositoryExtension>();
        }
    }
}