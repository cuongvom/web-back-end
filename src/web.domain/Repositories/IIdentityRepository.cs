using System.Threading.Tasks;
using web.domain.Model;
using web.domain.Models.RequestModel;

namespace web.domain.Repository
{
    public interface IIdentityRepository
    {
        Task<UserDto> LoginAsync(UserRequest userRequest);
        Task<UserDto> RegisterAsync(UserRequest userRequest);
        Task<bool> UserAnyAsync(UserRequest userRequest);
    }
}