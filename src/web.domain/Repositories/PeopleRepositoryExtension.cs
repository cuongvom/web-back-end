using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using web.domain.Context;
using web.domain.Entity;

namespace web.domain.Repository
{
    public class PeopleRepositoryExtension : IPeopleRepositoryExtension
    {
        private readonly FamilyDbContext _familyDbContext;

        public PeopleRepositoryExtension(FamilyDbContext familyDbContext)
        {
            _familyDbContext = familyDbContext;
        }

        public async Task<IEnumerable<People>> GetAllAsync()
        {
            return await _familyDbContext.Peoples
                                        .Include(x=>x.District)
                                        .Include(x=>x.District.Province).ToListAsync();
        }
    }
}