using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using web.domain.Context;
using web.domain.Model;
using web.domain.Models.RequestModel;

namespace web.domain.Repository
{
    public class IdentityRepository : IIdentityRepository
    {
        private readonly FamilyDbContext _familyDbContext;

        public IdentityRepository(FamilyDbContext familyDbContext)
        {
            _familyDbContext = familyDbContext;
        }

        public Task<UserDto> LoginAsync(UserRequest userRequest)
        {
            throw new System.NotImplementedException();
        }

        public Task<UserDto> RegisterAsync(UserRequest userRequest)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UserAnyAsync(UserRequest userRequest)
        {
            var passwordHash=GetMd5Hash(MD5.Create(),userRequest.Password);
            var isUser = await _familyDbContext.AppUsers.AnyAsync(user =>
            user.UserName == userRequest.UserName.ToLower() && VerifyMd5Hash(MD5.Create(),userRequest.Password,passwordHash));
            return isUser;
        }

        private string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
        private bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}