using System.Collections.Generic;
using System.Threading.Tasks;
using web.domain.Entity;

namespace web.domain.Repository
{
    public interface IPeopleRepositoryExtension
    {
         Task<IEnumerable<People>> GetAllAsync();
    }
}