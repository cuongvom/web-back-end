CREATE DATABASE self_study_sql

-----------TABLE
CREATE TABLE AppRole(
	Id int IDENTITY,
	RoleName nvarchar(50),
	PRIMARY KEY (Id)
)

CREATE TABLE AppUser(
	Id int IDENTITY,
	UserName nvarchar(50),
	RoleId int not null,
	Password varchar(50),
	PRIMARY KEY (Id),
	FOREIGN KEY (RoleId) REFERENCES AppRole(Id)
)

CREATE TABLE Province(
	Id int IDENTITY,
	Name nvarchar(50),
	PRIMARY KEY (Id)
)

CREATE TABLE District(
	Id int IDENTITY,
	Name nvarchar(50),
	ProvinceId int,
	PRIMARY KEY (Id),
	FOREIGN KEY (ProvinceId) REFERENCES Province(Id)
)

CREATE TABLE Family(
	Id int IDENTITY,
	HeadOfAHouseholdId int not null
	PRIMARY KEY (Id)
)

CREATE TABLE People(
	Id int IDENTITY,
	Name nvarchar(150),
	Age int,
	DistrictId int,
	IsMarried bit default 0,
	FamilyId int not null,
	PRIMARY KEY (Id),
	FOREIGN KEY (DistrictId) REFERENCES District(Id),
	FOREIGN KEY (FamilyId) REFERENCES Family(Id)
)

CREATE TABLE FamilyRelationship(
	PeopleId int,
	PeopleIdReId int,
	RelationshipName nvarchar(50),
	PRIMARY KEY (PeopleId),
	FOREIGN KEY (PeopleId) REFERENCES People(Id),
	FOREIGN KEY (PeopleIdReId) REFERENCES People(Id)
)

CREATE TABLE InsertLogForPeople(
	Id int IDENTITY,
	CreateBy int,
	CreateDate datetime,
	TriggerAction varchar(150)
	PRIMARY KEY (Id),
	FOREIGN KEY (CreateBy) REFERENCES People (id)
)
-------------VIEW

CREATE VIEW PeopleInfo
AS
SELECT p.Id AS Id, p.Name AS Name, p.Age, dis.Name As District, pro.Name AS Province
FROM People p LEFT JOIN
	District dis on p.DistrictId=dis.Id LEFT JOIN
	Province pro on dis.ProvinceId=pro.Id

-------------STORE PROCEDURE

CREATE PROCEDURE count_people_equal_age @age int
AS
    SELECT COUNT(*) as Result
    FROM People p
    WHERE p.Age = @age;

------------FUNCTION

CREATE FUNCTION GetAllPeopleByProvinceId (@provinceId int)
RETURNS TABLE
AS
RETURN(
	SELECT p.Id AS Id, p.Name AS Name, p.Age, dis.Name As District, pro.Name AS Province
	FROM People p LEFT JOIN
	District dis on p.DistrictId=dis.Id LEFT JOIN
	Province pro on dis.ProvinceId=pro.Id
	WHERE pro.Id=@provinceId
);
GO

ALTER FUNCTION GetHeadOfaHouseholdByPeopleId (@peopleId int)
RETURNS TABLE
AS
RETURN(
	SELECT f.Id AS FamilyId, p.Name AS HeadHouseholdName, p.Age, dis.Name As District, pro.Name AS Province
	FROM Family f LEFT JOIN 
	People p on p.FamilyId=f.Id LEFT JOIN
	District dis on p.DistrictId=dis.Id LEFT JOIN
	Province pro on dis.ProvinceId=pro.Id
	WHERE p.Id=@peopleId and f.HeadOfAHouseholdId=@peopleId
);
GO
------------TRIGGER

Alter TRIGGER ALTER_People_INSERT ON People
FOR INSERT
AS
	declare @createBy int;
	declare @triggerAction varchar(150);
	set @triggerAction='alter_people_insert';
	SELECT @createBy=p.Id
	FROM inserted p
	INSERT INTO [dbo].[InsertLogForPeople] (CreateBy, CreateDate,[TriggerAction])
			VALUES(@createBy,GETDATE(),@triggerAction);
	PRINT 'Alter People insert'
GO