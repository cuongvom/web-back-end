using Microsoft.AspNetCore.Mvc;
using web.domain.Service;

namespace web.application.api.Controllers
{
    [Route("family.angular/[controller]")]
    [ApiController]
    public class PeoplesController : ControllerBase
    {
        private readonly IPeopleService _peopleService;

        public PeoplesController(IPeopleService peopleService)
        {
            _peopleService = peopleService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var listPeople = _peopleService.GetAll();
            return Ok(new { data = listPeople });
        }
    }
}