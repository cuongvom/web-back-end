﻿using Microsoft.AspNetCore.Mvc;

namespace web.application.api.Controllers
{
    [Route("family.angular/[controller]")]
    [ApiController]
    public class IndexController : ControllerBase
    {
        [HttpGet]
        public string Index()
        {
            return "Welcome to my world.";
        }
    }
}